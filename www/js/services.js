angular.module('stockmarket.services', [])

.factory('encodeURIService', function() {

	return {
		encode: function(string) {
			console.log(string);
			return encodeURIComponent(string).replace(/\"/g, "%22").replace(/\ /g, "%20").replace(/[!'()]/g, escape);
		}
	};
})

.factory('dateService', function($filter) {

	var currentDate = function() {
		var d = new Date();
		var date = $filter('date')(d, 'yyyy-MM-dd');
		return date;
	};

	var oneYearAgoDate = function() {
		var d = new Date(new Date().setDate(new Date().getDate() - 365));
		var date = $filter('date')(d, 'yyyy-MM-dd');
		return date;
	};

	return {
		currentDate: currentDate,
		oneYearAgoDate: oneYearAgoDate
	}
})



.factory('chartDataCacheService', function(CacheFactory) {

	var chartDataCache;

	if(!CacheFactory.get('chartDataCache')) {

		chartDataCache = CacheFactory('chartDataCache', {
			maxAge: 60 * 60 * 8 * 1000,
			deleteOnExpire: 'aggressive',
			storageMode: 'localStorage'
		});
	}
	else {
		chartDataCache = CacheFactory.get('chartDataCache');
	}

	return chartDataCache;
})

.factory('stockDetailsCacheService', function(CacheFactory) {

	var stockDetailsCache;

	if(!CacheFactory.get('stockDetailsCache')) {

		stockDetailsCache = CacheFactory('stockDetailsCache', {
			maxAge: 60 * 60 * 8 * 1000,
			deleteOnExpire: 'aggressive',
			storageMode: 'localStorage'
		});
	}
	else {
		stockDetailsCache = CacheFactory.get('stockDetailsCache');
	}

	return stockDetailsCache;
})

.factory('notesCacheService', function(CacheFactory) {

	var notesCache;

	if(!CacheFactory.get('notesCache')) {

		notesCache = CacheFactory('notesCache', {
			storageMode: 'localStorage'
		});
	}
	else {
		notesCache = CacheFactory.get('notesCache');
	}

	return notesCache;
})

.factory('notesService', function(notesCacheService) {

	return {

		getNotes: function(ticker) {
			return notesCacheService.get(ticker);
		},

		addNote: function(ticker, note) {

			var stockNotes = [];

			if(notesCacheService.get(ticker)) {
				stockNotes = notesCacheService.get(ticker);
				stockNotes.push(note);
			} else {
				stockNotes.push(note);
			}


			notesCacheService.put(ticker, stockNotes);
		},

		deleteNote: function(ticker, index) {

			var stockNotes = [];

			stockNotes = notesCacheService.get(ticker);
			stockNotes.splice(index, 1);
			notesCacheService.put(ticker, stockNotes);
		}


	}
})

.factory('stockDataService', function($q, $http, encodeURIService, stockDetailsCacheService) {

	var getDetailsData = function(ticker) {
		var deferred = $q.defer();

		var cacheKey = ticker,
		stockDetailsCache = stockDetailsCacheService.get(cacheKey),

		query = 'select * from yahoo.finance.quotes where symbol IN("' + ticker + '")';
		var url = 'http://query.yahooapis.com/v1/public/yql?q=' + encodeURIService.encode(query) + '&format=json&env=http://datatables.org/alltables.env';

		console.log(url);

		if(stockDetailsCache) {

			deferred.resolve(stockDetailsCache);

		} else {

		$http.get(url)
			.success(function(json) {
				var jsonData = json.query.results.quote
				deferred.resolve(jsonData);
				stockDetailsCacheService.put(cacheKey, jsonData);
			})
			.error(function(error) {
				console.log("Details data error: " + error);
				deferred.reject();
			});

		}
		return deferred.promise;

	};

	var getPriceData = function(ticker) {

		var deferred = $q.defer(),
		url = "http://finance.yahoo.com/webservice/v1/symbols/" + ticker  + "/quote?format=json&view=detail";

		$http.get(url)
			.success(function(json) {
				var jsonData = json.list.resources[0].resource.fields
				deferred.resolve(jsonData);
			})
			.error(function(error) {
				console.log("Price data error: " + error);
				deferred.reject();
			});
			return deferred.promise;
	};

	return {
		getPriceData: getPriceData,
		getDetailsData: getDetailsData
	};
})

.factory('chartDataService', function($q, $http, encodeURIService, chartDataCacheService) {


	getHistoricalData = function(ticker, fromDate, todayDate) {

		var deferred = $q.defer();

		var cacheKey = ticker,
		chartDataCache = chartDataCacheService.get(cacheKey),

		query =  'select * from yahoo.finance.historicaldata where symbol = "' + ticker + '" and startDate = "' + fromDate + '" and endDate = "' + todayDate + '"';
		var url = 'http://query.yahooapis.com/v1/public/yql?q=' + encodeURIService.encode(query) + '&format=json&env=http://datatables.org/alltables.env';

		if(chartDataCache) {

			deferred.resolve(chartDataCache);

		} else {
			$http.get(url)
				.success(function(json) {
					var jsonData = json.query.results.quote;

					var priceData = [];
					var volumeData = [];

					jsonData.forEach(function(dayDataObject) {
						//console.log(dayDataObject);
						var datetoMillis = dayDataObject.Date;
						var date = Date.parse(datetoMillis);
						var price = parseFloat(Math.round(dayDataObject.Close * 100) / 100).toFixed(3);
						var volume = dayDataObject.Volume;

						var volumeDatum = '[' + date + ',' + volume + ']',
						priceDatum = '[' + date + ',' + price + ']';

						console.log(volumeDatum, priceDatum);

						volumeData.unshift(volumeDatum);
						priceData.unshift(priceDatum);
					});

					var formattedChartData =
					'[{' +
						'"key":' + '"volume",' +
						'"bar":' + 'true,' +
						'"values":' + '[' + volumeData + ']' +
					'},' +
					'{' +
						'"key":' + '"' + ticker + '",' +
						'"values":' + '[' + priceData + ']' +
					'}]';

					deferred.resolve(formattedChartData);
					chartDataCacheService.put(cacheKey, formattedChartData);
				})
				.error(function(error) {
					console.log("Chart data error: " + error);
					deferred.reject();
				});

			}
			return deferred.promise;

	}
	return {
		getHistoricalData: getHistoricalData
	};
})






;
