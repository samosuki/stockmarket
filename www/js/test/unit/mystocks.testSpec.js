describe('Testing StockMarket App', function(){

  beforeEach(module('stockmarket.controllers'));

  describe('Testing stockmarket Controller', function () {
    var scope, ctrl, httpBackend, timeout, rootScope;

    beforeEach(inject(function($controller, $rootScope, $httpBackend, $timeout) {
      rootScope = $rootScope;
      scope = $rootScope.$new();
      ctrl = $controller('MyStocksCtrl', {$scope:scope});
      httpBackend = $httpBackend;
      timeout = $timeout;
    }));

    afterEach(function() {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should initialize myStocksArray', function() {
      expect(scope.myStocksArray).toBeDefined();
    });
		it('should have myStocksArray as 6 stocks', function() {
      expect(scope.myStocksArray.length).toBe(6);
    });


	});
});
