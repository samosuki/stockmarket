describe('Testing StockMarket App', function(){

  beforeEach(module('stockmarket.controllers'));

  beforeEach(module(function ($provide) {

        // mock the entire $state provider
        $provide.provider('$state', function () {
            return {
                $get: function () {
                    return {
                        // by default it will be an empty object
                        params: {}
                    };
                }
            };
        });

    }));

  describe('Testing stockmarket Controller', function () {
    var scope, ctrl, httpBackend, timeout, rootScope, ionicPopup, stockDataService, dateService, chartDataService, notesService;

    beforeEach(inject(function($controller, $rootScope, $httpBackend, $timeout) {
      rootScope = $rootScope;
      scope = $rootScope.$new();
      ctrl = $controller('StockCtrl', {
        $scope:scope,
        $stateParams: { stockTicker: 1 },
        $ionicPopup: ionicPopup,
        stockDataService: stockDataService,
        dateService: dateService,
        chartDataService: chartDataService,
        notesService: notesService

      });
      httpBackend = $httpBackend;
      timeout = $timeout;

    }));

    afterEach(function() {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });



    //it('should could getNotes from stockDataService', function() {
    //  expect(notesService.getNotes).toHaveBeenCalledWith(4);
    //});

    describe('Testing chartView', function () {

      var dateSpy;


      beforeEach(function() {
        dateService = {
          oneYearAgoDate: function(value) {
            oneYearAgoDate = value;
          },
          currentDate: function(value) {
            currentDate = value;
          }
        }
        //dateSpy = spyOn(dateService, 'oneYearAgoDate').andReturn("2015-05-29");
      })
      it('should initialise notes', function() {
        expect(scope.stockNotes).toBeDefined();
        console.log(scope.stockNotes);
      });

      it('should initialise chartView', function() {
        //expect(dateService.oneYearAgoDate).toHaveBeenCalled();
        expect(scope.chartView).toEqual(4);
      });

      it('should update chartView', function() {
        scope.chartViewFunc(1);
        expect(scope.chartView).toEqual(1);
      });



    });

    //it('should could getNotes from stockDataService', function() {
    //  expect(notesService.getNotes).toHaveBeenCalledWith(4);
    //});


	});
});
